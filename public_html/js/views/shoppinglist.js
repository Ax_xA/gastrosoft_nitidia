// site/js/views/library.js

var app = app || {};

app.ListView = Backbone.View.extend({
    el: '#items',

    initialize: function( initialBooks ) {
        this.collection = new app.List( initialBooks );
        this.render();
    },

    // render library by rendering each book in its collection
    render: function() {
        this.collection.each(function( item ) {
            this.renderBook( item );
        }, this );
    },

    // render a book by creating a BookView and appending the
    // element it renders to the library's element
    renderBook: function( item ) {
        var itemView = new app.ItemView({
            model: item
        });
        this.$el.append( itemView.render().el );
    }
});